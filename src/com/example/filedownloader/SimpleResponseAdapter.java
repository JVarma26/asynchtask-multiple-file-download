package com.example.filedownloader;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


public class SimpleResponseAdapter extends BaseAdapter implements
		OnClickListener {

	// FileDownloader mFileDownloader;
	ProgressBar mProgressBar;
	private Context mContext;
	private List<ListDataModel> mArrayList;
	private Button cancelButton, showButton;
	
	private static class ViewHolder {
	}

	ViewHolder viewHolder = null;

	public SimpleResponseAdapter(Context context, List<ListDataModel> listItem) {
		this.mContext = context;
		this.mArrayList = listItem;
	}

	@Override
	public int getCount() {
		return mArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return mArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;

		// reuse Views
		if (rowView == null) {

			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.listview, null);

			viewHolder = new ViewHolder();

			rowView.setTag(viewHolder);
			// Log.d("TAG", "SETTAG");
		} else {
			viewHolder = (ViewHolder) rowView.getTag();

			// Log.d("TAG", "GETTag");
		}
		TextView fileName = (TextView) rowView.findViewById(R.id.file_name);
		TextView downloadStatus = (TextView) rowView
				.findViewById(R.id.file_download_status);
		ProgressBar mProgressBar = (ProgressBar) rowView
				.findViewById(R.id.progress_bar);

		cancelButton = (Button) rowView.findViewById(R.id.button_cancel);
		cancelButton.setTag(position);
		cancelButton.setOnClickListener(this);

		showButton = (Button) rowView.findViewById(R.id.button_show);
		showButton.setTag(position);
		showButton.setOnClickListener(this);
		
		
		fileName.setText(mArrayList.get(position).getmFilename());

		mProgressBar.setProgress(mArrayList.get(position).getmProgress());
		
		if(mArrayList.get(position).getmProgress() == 100){
			cancelButton.setVisibility(View.GONE);
			showButton.setVisibility(View.VISIBLE);
		};

		downloadStatus.setText(""
				+ mArrayList.get(position).getmFileDownlodStatus() + "%");

		return rowView;
	}
	
	@Override
	public void onClick(View v) {
		
		switch(v.getId()){
		
		case R.id.button_cancel:
			//////
			int pos = (int) v.getTag();
			Log.i("TAG", pos + "");
			ListDataModel dataModel = (ListDataModel) getItem(pos);

//			Log.i("TAG", dataModel.getDownloader().isCancelled() + "");

			if (!dataModel.getDownloader().isCancelled()) {
				dataModel.getDownloader().cancel(true);
			}
			break;
		case R.id.button_show:
				openFolder();
				break;
		default:
			break;
			
		}
	}

	public void openFolder() {
		File file = new File(Environment.getExternalStorageDirectory(), "Downloader_Clarice");
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setDataAndType(Uri.fromFile(file), "*/*");
		mContext.startActivity(intent);
	}



}
//// Alert Dialog
//AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//		mContext);
//alertDialogBuilder
//		.setMessage("Do you want to Cancel/Download")
//		.setCancelable(false)
//		.setPositiveButton("Download",
//				new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,
//							int id) {
//
//					}
//				})
//		.setNegativeButton("Cancel",
//				new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,
//							int id) {
////							mArrayList.remove(pos);
//							dataModel.getDownloader().cancel(true);
//							dialog.cancel();
//					}
//				});
//
//AlertDialog alertDialog = alertDialogBuilder.create();
//alertDialog.show();
//}	
/////
