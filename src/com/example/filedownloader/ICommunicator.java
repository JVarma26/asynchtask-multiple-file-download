package com.example.filedownloader;

public interface ICommunicator {

	void callbackSuccess();
	void callbackFailure();
	void onProgressUpdate(int pos, int progress);
}
