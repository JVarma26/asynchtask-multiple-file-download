package com.example.filedownloader;


public class ListDataModel {

	public String mFilename;
	private String mFileLocation;
	private int mProgress;
	private String mUrl;
	private int mFileDownlodStatus;
	private FileDownloader downloader;
	private boolean isCancelled;
//	public String mFilePos;

	public FileDownloader getDownloader() {
		return downloader;
	}

	public void setDownloader(FileDownloader downloader) {
	
		this.downloader = downloader;
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	public int getmFileDownlodStatus() {
		return mFileDownlodStatus;
	}

	public void setmFileDownlodStatus(int mFileDownlodStatus) {
		this.mFileDownlodStatus = mFileDownlodStatus;
	}

	public int getmProgress() {
		return mProgress;
	}

	public void setmProgress(int mProgress) {
		this.mProgress = mProgress;
	}

	public String getmUrl() {
		return mUrl;
	}

	public void setmUrl(String mUrl) {
		this.mUrl = mUrl;
	}


	public ListDataModel() {

	}


	public ListDataModel(ListDataModel l) {

		this.mFilename=l.mFilename;
		this.mFileLocation=l.mFileLocation;
		this.mProgress = l.mProgress;
		this.mUrl = l.mUrl;
		this.mFileDownlodStatus = l.mFileDownlodStatus;
		this.downloader=l.downloader;
		this.isCancelled=l.isCancelled;

	}

	public String getmFilename() {
		return mFilename;
	}

	public void setmFilename(String mFilename) {
		this.mFilename = mFilename;
	}

	public String getmFileLocation() {
		return mFileLocation;
	}

	public void setmFileLocation(String mFileLocation) {
		this.mFileLocation = mFileLocation;
	}

}
