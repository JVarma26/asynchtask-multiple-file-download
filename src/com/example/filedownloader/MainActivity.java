package com.example.filedownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.example.filedownloader.FileDownloader.Status;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity {

	private List<ListDataModel> mListArray;
	private Button mButtonDownload;
	private EditText mFileName, mUrl;
	private ListView mListView;

	private SimpleResponseAdapter adapter;

	// private FileDownloader mFileDownloader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mButtonDownload = (Button) findViewById(R.id.button1);
		mFileName = (EditText) findViewById(R.id.file_name);
		mUrl = (EditText) findViewById(R.id.url);
		mListView = (ListView) findViewById(R.id.listView1);

		mListArray = new ArrayList<ListDataModel>();

		adapter = new SimpleResponseAdapter(this, mListArray);
		mListView.setAdapter(adapter);
		// mFileDownloader = ;

		mButtonDownload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				File SDCardRoot = new File(Environment
						.getExternalStoragePublicDirectory(""),
						"Downloader_Clarice");
				File file = new File(SDCardRoot.getPath(), mFileName.getText()
						.toString());

				if (file.exists()) {

					// Alert Dialog
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							MainActivity.this);
					alertDialogBuilder
							.setMessage("File Name is Already exists")

							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, just
											// close
											// the dialog box and do nothing
											dialog.cancel();
										}
									});

					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
					return;
				}

				// ////////////////////////////
				ListDataModel mListDataModel = new ListDataModel();

				mListDataModel.setmFilename(mFileName.getText().toString());
				mListDataModel.setmUrl(mUrl.getText().toString());
				mListDataModel.setmProgress(0);
				mListDataModel.setmFileDownlodStatus(0);
				mListDataModel.setmFileLocation("");
				mListDataModel.setCancelled(false);
				
				String[] req = {
						"http://128.downloadming1.com/bollywood%20mp3/A%20Strange%20Love%20Story%20%282011%29/01%20-%20Nede%20Nede.mp3",
						mFileName.getText().toString() };

//				String[] req = {
//						mUrl.getText().toString(),
//						mFileName.getText().toString() };

				
				mListDataModel
						.setDownloader((FileDownloader) new FileDownloader(
								communicator, mListArray.size(), adapter)
								.executeOnExecutor(
										AsyncTask.THREAD_POOL_EXECUTOR, req));
//				
//				mListDataModel
//				.setDownloader((FileDownloader) new FileDownloader(
//						communicator, mListArray.size(), adapter)
//						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR ,req));
//				
//				FileDownloader loader =new FileDownloader(communicator, mListArray.size(), adapter);
//				AsyncTask<String, Integer, Status> task =loader.execute(req);
//				task.

			
				// mListDataModel.setDownloader((FileDownloader) new
				// FileDownloader(communicator, mListArray.size(),adapter)
				// .execute(req));
				// mListDataModel.setCancelled(false);

				mListArray.add(mListDataModel);

				adapter.notifyDataSetChanged();

			}
		});
	}

	ICommunicator communicator = new ICommunicator() {

		@Override
		public void callbackSuccess() {
		}

		@Override
		public void callbackFailure() {

		}

		@Override
		public void onProgressUpdate(int pos, int progress) {
			mListArray.get(pos).setmProgress(progress);
			mListArray.get(pos).setmFileDownlodStatus(progress);
			adapter.notifyDataSetChanged();
		}
	};

}