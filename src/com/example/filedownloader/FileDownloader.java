package com.example.filedownloader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import com.example.filedownloader.FileDownloader.Status;

public class FileDownloader extends AsyncTask<String, Integer, Status> {

	ICommunicator mInterface;
	ListDataModel mListDataModel;
	private int position;
	private Context mContext;

	SimpleResponseAdapter adapter;

	public FileDownloader(ICommunicator mInterface, int position,
			SimpleResponseAdapter adapter) {
		super();
		this.mInterface = mInterface;
		this.position = position;
		this.adapter = adapter;
	}

	@Override
	protected Status doInBackground(String... param) {
		
		

		// //////////////////
		try {
			URL url = new URL(param[0]);
			String FileName = param[1];

			URLConnection connection = url.openConnection();
			connection.connect();
			// create a new file, to save the downloaded file
			File SDCardRoot = new File(
					Environment.getExternalStoragePublicDirectory(""),
					"Downloader_Clarice");

			File file = new File(SDCardRoot.getPath(), FileName);

			if (!SDCardRoot.exists()) {
				if (!SDCardRoot.mkdirs()) {
					// Log.d("Downloader_Clarice",
					// "failed to create directory");
					// Toast.makeText(mContext, "Already Exists",
					// Toast.LENGTH_SHORT).show();
					return null;
				}
			}

			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream input = new BufferedInputStream(url.openStream());
			
			int totalSize = connection.getContentLength();
			// mListDataModel = (ListDataModel) adapter.getItem(position);

			byte[] buffer = new byte[1024];

			int bufferLength = 0;
			int downloadedSize = 0;
			System.out.println("pre" + totalSize + ":" + downloadedSize);
			// Toast.makeText(mContext, ""+totalSize+":"+downloadedSize,
			// Toast.LENGTH_LONG).show();
			
			while (((bufferLength = input.read(buffer)) > 0)) {
				System.out.println("dOWNLOAD" + totalSize + ":"
						+ downloadedSize);
				// Log.d("TAG", "Iscanceled:-" + mListDataModel.isCancelled());
				// Toast.makeText(mContext, ""+totalSize+":"+downloadedSize,
				// Toast.LENGTH_LONG).show();
				downloadedSize += bufferLength;

				publishProgress((int) ((downloadedSize * 100) / totalSize));

				fileOutput.write(buffer, 0, bufferLength);

			}
			// System.out.println( "POST"+totalSize+":"+downloadedSize);
			fileOutput.close();

		} catch (final MalformedURLException e) {
			// showError("Error : MalformedURLException " + e);
			e.printStackTrace();
			return Status.FAILURE;
		} catch (final IOException e) {
			// showError("Error : IOException " + e);
			e.printStackTrace();
			return Status.FAILURE;
		} catch (final Exception e) {
			// showError("Error : Please check your internet connection " + e);
			e.printStackTrace();
			return Status.FAILURE;
		}

		if (isCancelled() == true);

		return Status.SUCCESS;

	}

	@Override
	protected void onPostExecute(Status result) {
		super.onPostExecute(result);
		if (result == Status.SUCCESS) {
			mInterface.callbackSuccess();
		} else {
			mInterface.callbackFailure();
		}

	}

	@Override
	protected void onCancelled() {
		// TODO Auto-generated method stub
		super.onCancelled();
		FileDownloader.this.cancel(true);
	}

	@Override
	protected void onCancelled(Status result) {
		// TODO Auto-generated method stub
		super.onCancelled(result);
		mInterface.callbackFailure();

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	// public void setPosition(int pos) {
	// this.position = pos;
	// }

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		mInterface.onProgressUpdate(position, values[0]);
	}

	public enum Status {
		SUCCESS, FAILURE
	}

}
